# BCC SPARROW

## Project information

This project is part of the SPARROW AI accelerator developed by Marc Solé Bonet under the supervision of Dr. Leonidas Kosmidis.

## Related repositories

- [Main SPARROW repository](https://gitlab.bsc.es/msolebon/sparrow)
- [LLVM compiler](https://gitlab.bsc.es/msolebon/llvm-sparrow)

## File organization

The repository contains the source files for Cobham Gaisler's BCC-2.2.1, and are organized as such.

The modifications to support SPARROW instructions are found under the binutils directory in the following path:
    
- `./binutils/include/opcode/sparc.h`
- `./binutils/include/elf/sparc.h`
- `./binutils/opcodes/sparc-dis.c`
- `./binutils/opcodes/sparc-opc.c`
- `./binutils/gas/config/tc-sparc.c`

All the modifications are tagged with *SPARROW* comments to ease their localization.

## Build

The *ubuild.sh* script is provided to simplify the configuration and building of the compiler.

Use `./ubuild.sh --destination /path/to/target/directory --toolchain --libbcc`

The use of *gdb*, although included, has not been extended nor tested for the sparrow instructions.

## Prerequisites

The prerequisites for installing the modified bcc compiler are the same ones as for gcc. 
It is necessary to have installed `flex` and `bison`, which can be done through the package manager.
For the installation of gcc is also required the `gmp`, `mpc` and `mpfr` libraries.

It is recommended to install the prerequisites manually, however they can also be built using the `--prereq`
flag when executing the *ubuild.sh* script.

Doing so requires administrator permission unless the script is modified to use a user path.
Depending on the distribution other modifications might be necessary.

## License

This repository contains a modification of the [GNU BCC2 Cross-Compilation system by Cobham Gaisler](https://www.gaisler.com/index.php/products/operating-systems/bcc). Which allows the generation of code for SPARC v8 processors in particular for the LEON3.

The original files can be found [here](https://www.gaisler.com/index.php/downloads/compilers). 

The contents of this repository are distributed under a GPL license. If you use this work or any part of it, please cite it as:

*Marc Solé Bonet, Leonidas Kosmidis, SPARROW: A Low-Cost Hardware/Software Co-designed SIMD Microarchitecture for AI Operations in Space Processors. Design, Automation and Test in Europe Conference, DATE 2022*
